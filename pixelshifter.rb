require 'rubygems'
require 'rmagick'

def rgb_to_hsl(red, green, blue)
    red /= 255.to_f
    green /= 255.to_f
    blue /= 255.to_f

    hue = 0.to_f
    saturation = 0.to_f
    lightness = 0.to_f

    minimum = [red, green, blue].min
    maximum = [red, green, blue].max
    delta_max = maximum - minimum

    lightness = (maximum + minimum) / 2

    if delta_max == 0 #gray
        hue = 0
        saturation = 0
    else
        if lightness < 0.5
            saturation = delta_max / (maximum + minimum)
        else
            saturation = delta_max / (2 - maximum - minimum)
        end

        delta_red = (((maximum - red) / 6) + (maximum / 2)) / delta_max
        delta_green = (((maximum - green) / 6) + (maximum / 2)) / delta_max
        delta_blue = (((maximum - blue) / 6) + (maximum / 2)) / delta_max

        if red == maximum
            hue = delta_blue - delta_green
        elsif green == maximum
            hue = (1 / 3) + delta_red - delta_blue
        elsif blue == maximum
            hue = (2 / 3) + delta_green - delta_red
        end

        if hue < 0
            hue += 1
        end

        if hue > 1
            hue -= 1
        end
    end

    return [hue, saturation, lightness]
end

# Store incoming parameters
inputFile1 = ARGV[0]
outputFile = ARGV[1]
inputFile2 = ARGV[2] ? ARGV[2] : false

img1 = Magick::Image::read(inputFile1)[0]
w = img1.columns
h = img1.rows

coordsArray = Hash.new
colorArray1 = Array.new
for y in 0..h
    for x in 0..w
        pixel = img1.pixel_color(x, y)
        colorArray1.push([rgb_to_hsl(pixel.red, pixel.green, pixel.blue), pixel, [x, y]])
    end
end

if inputFile2 then
    colorArray1.sort_by! do |item|
        item[0][2]
    end
else
    colorArray1.sort_by! do |item|
        item[0][0]
    end
end

arrayIndex1 = 0
for y in 0..h
    for x in 0..w
        if inputFile2 then
            unless coordsArray.has_key?(x.to_s) then
                coordsArray[x.to_s] = Hash.new
            end

            coordsArray[x.to_s][y.to_s] = [colorArray1[arrayIndex1][2][0], colorArray1[arrayIndex1][2][1]]
        else
            img1.pixel_color(x, y, colorArray1[arrayIndex1][1])
        end
        arrayIndex1 += 1
    end
end
colorArray1 = nil

# Ready for phase 2?
if inputFile2 then
    # Reset img1 to new image to prevent memory issues
    img1 = nil
    img3 = Magick::Image.new(w, h)
    img2 = Magick::Image::read(inputFile2)[0]
    w2 = img2.columns
    h2 = img2.rows

    # Make sure img2 has the same dimensions as img1
    if(w != w2 || h != h2) then
        img2.resize!(w, h)
    end

    colorArray2 = Array.new
    for y in 0..h
        for x in 0..w
            pixel = img2.pixel_color(x, y)
            colorArray2.push([rgb_to_hsl(pixel.red, pixel.green, pixel.blue), pixel, [x, y]])
        end
    end
    img2 = nil

    colorArray2.sort_by! do |item|
        item[0][2]
    end

    arrayIndex2 = 0
    for y in 0..h
        for x in 0..w
            img3.pixel_color(coordsArray[x.to_s][y.to_s][0], coordsArray[x.to_s][y.to_s][1], colorArray2[arrayIndex2][1])
            arrayIndex2 += 1
        end
    end

    # Output file
    img3.write(outputFile)

else
    # Output file
    img1.write(outputFile)
end
