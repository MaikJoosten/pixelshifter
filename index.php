<?php
$inputDir = dirname(__FILE__) . '/input/';
$outputDir = dirname(__FILE__) . '/output/';
$outputDirWeb = 'output/';
$msg = '';

// Convert string to safe url string
function string2url($url, $lowercase = true){
    $transliterationTable = array('á' => 'a', 'Á' => 'A', 'à' => 'a', 'À' => 'A', 'ă' => 'a', 'Ă' => 'A', 'â' => 'a', 'Â' => 'A', 'å' => 'a', 'Å' => 'A', 'ã' => 'a', 'Ã' => 'A', 'ą' => 'a', 'Ą' => 'A', 'ā' => 'a', 'Ā' => 'A', 'ä' => 'ae', 'Ä' => 'AE', 'æ' => 'ae', 'Æ' => 'AE', 'ḃ' => 'b', 'Ḃ' => 'B', 'ć' => 'c', 'Ć' => 'C', 'ĉ' => 'c', 'Ĉ' => 'C', 'č' => 'c', 'Č' => 'C', 'ċ' => 'c', 'Ċ' => 'C', 'ç' => 'c', 'Ç' => 'C', 'ď' => 'd', 'Ď' => 'D', 'ḋ' => 'd', 'Ḋ' => 'D', 'đ' => 'd', 'Đ' => 'D', 'ð' => 'dh', 'Ð' => 'Dh', 'é' => 'e', 'É' => 'E', 'è' => 'e', 'È' => 'E', 'ĕ' => 'e', 'Ĕ' => 'E', 'ê' => 'e', 'Ê' => 'E', 'ě' => 'e', 'Ě' => 'E', 'ë' => 'e', 'Ë' => 'E', 'ė' => 'e', 'Ė' => 'E', 'ę' => 'e', 'Ę' => 'E', 'ē' => 'e', 'Ē' => 'E', 'ḟ' => 'f', 'Ḟ' => 'F', 'ƒ' => 'f', 'Ƒ' => 'F', 'ğ' => 'g', 'Ğ' => 'G', 'ĝ' => 'g', 'Ĝ' => 'G', 'ġ' => 'g', 'Ġ' => 'G', 'ģ' => 'g', 'Ģ' => 'G', 'ĥ' => 'h', 'Ĥ' => 'H', 'ħ' => 'h', 'Ħ' => 'H', 'í' => 'i', 'Í' => 'I', 'ì' => 'i', 'Ì' => 'I', 'î' => 'i', 'Î' => 'I', 'ï' => 'i', 'Ï' => 'I', 'ĩ' => 'i', 'Ĩ' => 'I', 'į' => 'i', 'Į' => 'I', 'ī' => 'i', 'Ī' => 'I', 'ĵ' => 'j', 'Ĵ' => 'J', 'ķ' => 'k', 'Ķ' => 'K', 'ĺ' => 'l', 'Ĺ' => 'L', 'ľ' => 'l', 'Ľ' => 'L', 'ļ' => 'l', 'Ļ' => 'L', 'ł' => 'l', 'Ł' => 'L', 'ṁ' => 'm', 'Ṁ' => 'M', 'ń' => 'n', 'Ń' => 'N', 'ň' => 'n', 'Ň' => 'N', 'ñ' => 'n', 'Ñ' => 'N', 'ņ' => 'n', 'Ņ' => 'N', 'ó' => 'o', 'Ó' => 'O', 'ò' => 'o', 'Ò' => 'O', 'ô' => 'o', 'Ô' => 'O', 'ő' => 'o', 'Ő' => 'O', 'õ' => 'o', 'Õ' => 'O', 'ø' => 'oe', 'Ø' => 'OE', 'ō' => 'o', 'Ō' => 'O', 'ơ' => 'o', 'Ơ' => 'O', 'ö' => 'oe', 'Ö' => 'OE', 'ṗ' => 'p', 'Ṗ' => 'P', 'ŕ' => 'r', 'Ŕ' => 'R', 'ř' => 'r', 'Ř' => 'R', 'ŗ' => 'r', 'Ŗ' => 'R', 'ś' => 's', 'Ś' => 'S', 'ŝ' => 's', 'Ŝ' => 'S', 'š' => 's', 'Š' => 'S', 'ṡ' => 's', 'Ṡ' => 'S', 'ş' => 's', 'Ş' => 'S', 'ș' => 's', 'Ș' => 'S', 'ß' => 'SS', 'ť' => 't', 'Ť' => 'T', 'ṫ' => 't', 'Ṫ' => 'T', 'ţ' => 't', 'Ţ' => 'T', 'ț' => 't', 'Ț' => 'T', 'ŧ' => 't', 'Ŧ' => 'T', 'ú' => 'u', 'Ú' => 'U', 'ù' => 'u', 'Ù' => 'U', 'ŭ' => 'u', 'Ŭ' => 'U', 'û' => 'u', 'Û' => 'U', 'ů' => 'u', 'Ů' => 'U', 'ű' => 'u', 'Ű' => 'U', 'ũ' => 'u', 'Ũ' => 'U', 'ų' => 'u', 'Ų' => 'U', 'ū' => 'u', 'Ū' => 'U', 'ư' => 'u', 'Ư' => 'U', 'ü' => 'ue', 'Ü' => 'UE', 'ẃ' => 'w', 'Ẃ' => 'W', 'ẁ' => 'w', 'Ẁ' => 'W', 'ŵ' => 'w', 'Ŵ' => 'W', 'ẅ' => 'w', 'Ẅ' => 'W', 'ý' => 'y', 'Ý' => 'Y', 'ỳ' => 'y', 'Ỳ' => 'Y', 'ŷ' => 'y', 'Ŷ' => 'Y', 'ÿ' => 'y', 'Ÿ' => 'Y', 'ź' => 'z', 'Ź' => 'Z', 'ž' => 'z', 'Ž' => 'Z', 'ż' => 'z', 'Ż' => 'Z', 'þ' => 'th', 'Þ' => 'Th', 'µ' => 'u', 'а' => 'a', 'А' => 'a', 'б' => 'b', 'Б' => 'b', 'в' => 'v', 'В' => 'v', 'г' => 'g', 'Г' => 'g', 'д' => 'd', 'Д' => 'd', 'е' => 'e', 'Е' => 'e', 'ё' => 'e', 'Ё' => 'e', 'ж' => 'zh', 'Ж' => 'zh', 'з' => 'z', 'З' => 'z', 'и' => 'i', 'И' => 'i', 'й' => 'j', 'Й' => 'j', 'к' => 'k', 'К' => 'k', 'л' => 'l', 'Л' => 'l', 'м' => 'm', 'М' => 'm', 'н' => 'n', 'Н' => 'n', 'о' => 'o', 'О' => 'o', 'п' => 'p', 'П' => 'p', 'р' => 'r', 'Р' => 'r', 'с' => 's', 'С' => 's', 'т' => 't', 'Т' => 't', 'у' => 'u', 'У' => 'u', 'ф' => 'f', 'Ф' => 'f', 'х' => 'h', 'Х' => 'h', 'ц' => 'c', 'Ц' => 'c', 'ч' => 'ch', 'Ч' => 'ch', 'ш' => 'sh', 'Ш' => 'sh', 'щ' => 'sch', 'Щ' => 'sch', 'ъ' => '', 'Ъ' => '', 'ы' => 'y', 'Ы' => 'y', 'ь' => '', 'Ь' => '', 'э' => 'e', 'Э' => 'e', 'ю' => 'ju', 'Ю' => 'ju', 'я' => 'ja', 'Я' => 'ja');
    $url = str_replace(array_keys($transliterationTable), array_values($transliterationTable), $url);
    $url = html_entity_decode(iconv('UTF-8', 'US-ASCII//TRANSLIT', $url));
    $url = preg_replace(array('/[\s\/]+/', '/[^a-z0-9_\-]/i', '/\-+/'), array('-', '', '-'), $url);
    $url = trim($url, '-');
    if($lowercase) $url = strtolower($url);
    return $url;
}

if(isset($_FILES['image1']) && $_FILES['image1']['size'] > 0) {

    // Get file info
    $fileInfo = getimagesize($_FILES['image1']['tmp_name']);
    if($fileInfo === false) {
       $msg = 'Unable to determine image type of uploaded file';

    // Check image type
    } elseif(($fileInfo[2] !== IMAGETYPE_GIF) && ($fileInfo[2] !== IMAGETYPE_JPEG) && ($fileInfo[2] !== IMAGETYPE_PNG)) {
       $msg = 'Your file is not an image!';

    // Check image size
    } elseif(($fileInfo[0] > 2000) || ($fileInfo[1] > 2000)) {
       $msg = 'Your file exceeds the 2000 x 2000 px limit!';

    // Limit to 2MB files
    } elseif($_FILES['image1']['size'] >  2 * 1024 * 1024) {
        $msg = 'Your file exceeds the 2MB filesize limit!';

    // All good; continue!
    } else {
        $filename = string2url(pathinfo($_FILES['image1']['name'], PATHINFO_FILENAME), false);
        $ext = pathinfo($_FILES['image1']['name'], PATHINFO_EXTENSION);

        $rand = md5(time() * rand());
        $tempname = $inputDir . $rand . '.' . $ext;

        // Save file
        if(move_uploaded_file($_FILES['image1']['tmp_name'], $tempname)) {
            $output = $outputDir . $filename . '_' . $rand . '.' . $ext;
            $outputWeb = $outputDirWeb . $filename . '_' . $rand . '.' . $ext;

            if(isset($_FILES['image2']) && $_FILES['image2']['size'] > 0) {

                // Get file info
                $fileInfo = getimagesize($_FILES['image2']['tmp_name']);
                if($fileInfo === false) {
                   $msg = 'Unable to determine image type of second uploaded file';

                // Check image type
                } elseif(($fileInfo[2] !== IMAGETYPE_GIF) && ($fileInfo[2] !== IMAGETYPE_JPEG) && ($fileInfo[2] !== IMAGETYPE_PNG)) {
                   $msg = 'Your second file is not an image!';

                // Check image size
                } elseif(($fileInfo[0] > 2000) || ($fileInfo[1] > 2000)) {
                   $msg = 'Your second file exceeds the 2000 x 2000 px limit!';

                // Limit to 2MB files
                } elseif($_FILES['image2']['size'] >  2 * 1024 * 1024) {
                    $msg = 'Your second file exceeds the 2MB filesize limit!';

                // All good; continue!
                } else {
                    $filename = string2url(pathinfo($_FILES['image2']['name'], PATHINFO_FILENAME), false);
                    $ext = pathinfo($_FILES['image2']['name'], PATHINFO_EXTENSION);

                    $rand = md5(time() * rand());
                    $tempname2 = $inputDir . $rand . '.' . $ext;

                    // Save file
                    if(move_uploaded_file($_FILES['image2']['tmp_name'], $tempname2)) {
                        $output = $outputDir . $filename . '_' . $rand . '.' . $ext;
                        $outputWeb = $outputDirWeb . $filename . '_' . $rand . '.' . $ext;

                        // Convert image
                        system('ruby pixelshifter.rb "' . $tempname . '" "' . $output . '" "' . $tempname2 . '"');
                    }
                }

            } else {

                // Convert image
                system('ruby pixelshifter.rb "' . $tempname . '" "' . $output . '"');
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>PixelShifter</title>
        <meta name="description" content="Let me sort that image out for ya!">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:site_name" content="PixelShifter" />
        <meta property="og:title" content="PixelShifter" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="Let me sort that image out for ya!" />
        <meta property="og:url" content="http://pixelshifter.nimmenemaal.com/" />
        <meta property="og:image" content="http://pixelshifter.nimmenemaal.com/share_image.jpg"/>
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <script type="text/javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>
    </head>
    <body>
        <form action="" method="post" id="form" enctype="multipart/form-data">
            <div id="confirm">
                <div class="title">Want to double your fun?</div>
                <div class="btn" id="yes">YES!</div>
                <div class="btn" id="no">Nah…</div>
            </div>
            <?php if(isset($outputWeb)) { ?>

            <a href="<?=$outputWeb?>" id="output" download style="background-image: url(<?=$outputWeb?>);"><div>Click here to download the result</div></a>

            <?php } else { ?>

            <div id="instructions">
                Drag a file onto here to start<br />
                Or click here to select a file<br />
                <div class="small">(max. 2MB or 2000 x 2000 px)</div>
                <div class="error"><?=$msg?></div>
            </div>
            <div id="loading">Give me a second to upload and convert the image…</div>
            <input type="file" name="image1" id="image1" accept="image/jpeg" />
            <input type="file" name="image2" id="image2" accept="image/jpeg" />

            <?php } ?>
        </form>
        <script type="text/javascript">
            $(document).ready(function() {
                var confirm = $('#confirm'),
                    image2 = $('#image2'),
                    form = $('#form');

                $('#image1').on('change', function(e) {
                    $(this).hide();
                    confirm.show();

                    $('#yes').on('click', function() {
                        image2.show();
                        confirm.hide();
                    });
                    $('#no').on('click', function() {
                        $('body').addClass('uploading');
                        confirm.hide();
                        form.submit();
                    });
                });
                image2.on('change', function(e) {
                    $(this).hide();
                    $('body').addClass('uploading');
                    form.submit();
                });
            });
        </script>
    </body>
</html>